const { Snippets } = require('gitlab');
const Debug = require('debug');

const { name } = require('../../package.json');

const debug = Debug(`${name}:middleware-gitlab`);

const shouldFetchById = client => id => client.content(id);

const shouldFetchByTitle = client => title => client.all()
  .then((snippets) => {
    const [snippet] = snippets.filter(_ => _.title === title);

    return snippet;
  });

module.exports = (ctx) => {
  const {
    token, endpoint: url, configId: id, configName: title,
  } = ctx.get('service.gitlab');

  if (!token) throw new TypeError('Token is not defined');

  const opts = { url: url || undefined, token };

  const snippets = new Snippets(opts);

  debug('Initialized with config:', opts);

  const fetchById = shouldFetchById(snippets);
  const fetchByTitle = shouldFetchByTitle(snippets);

  return (req, res) => {
    let shouldFetch;

    if (id) {
      shouldFetch = fetchById(id);
    } else if (title) {
      shouldFetch = fetchByTitle(title);
    } else {
      return res.sendStatus(501);
    }

    return shouldFetch
      .then((maybeResponse) => {
        process.env.LOG_LEVEL === 'debug' && debug('Got response', maybeResponse);
        if (!maybeResponse) return res.sendStatus(500);

        return id ? maybeResponse : fetchById(maybeResponse.id);
      })
      .then(content => res.send(content))
      .catch((error) => {
        process.env.LOG_LEVEL === 'debug' && debug('Got error', error);
        res.sendStatus(500);
      });
  };
};
